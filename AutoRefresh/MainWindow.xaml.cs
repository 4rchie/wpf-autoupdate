﻿using System;
using System.Windows;

namespace AutoRefresh
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DomainEventDispatcher.Get().Publish("CurrentTime", DateTime.Now.ToShortTimeString());
        }
    }
}
