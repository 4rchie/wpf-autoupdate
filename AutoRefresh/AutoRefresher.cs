﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace AutoRefresh
{
    public class AutoRefresher : Behavior<UIElement>
    {

        protected override void OnAttached()
        {
            
            var textBox = AssociatedObject as TextBox;

            if(textBox != null )
                Subscribe(textBox, Token);
        }

        private static void Subscribe(UIElement uiElement, string token)
        {
            var dispatcher = DomainEventDispatcher.Get();

            dispatcher.Subscribe(new TextBoxConverter(uiElement), token);
        }

        public static readonly DependencyProperty TokenProperty = DependencyProperty.Register("Token", typeof(string), typeof(AutoRefresher), new PropertyMetadata(default(string)));

        public string Token
        {
            get { return (string)GetValue(TokenProperty); }
            set { SetValue(TokenProperty, value); }
        }
    }
}