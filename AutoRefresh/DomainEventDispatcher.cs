﻿using System.Collections.Generic;

namespace AutoRefresh
{
    public class DomainEventDispatcher
    {
        private static DomainEventDispatcher _dispatcher;

        private DomainEventDispatcher()
        {
            Subscribers = new Dictionary<string, List<UiConverter>>();
        }

        public static DomainEventDispatcher Get()
        {
            return _dispatcher ?? (_dispatcher = new DomainEventDispatcher());
        }

        public void Subscribe(UiConverter converter, string token)
        {

            if (Subscribers.ContainsKey(token))
            {
                var subscribers = Subscribers[token];
                subscribers.Add(converter);
            }
            else
            {
                Subscribers.Add(token,new List<UiConverter>(new[]{converter}));
            }
        }

        private  Dictionary<string, List<UiConverter>> Subscribers { get; set; }

        public void Publish(string token, string value)
        {

            if (Subscribers.ContainsKey(token))
            {
                var subscribers = Subscribers[token];

                foreach (var subscriber in subscribers)
                {
                    subscriber.Set(value);
                }
            }

        }
    }
}