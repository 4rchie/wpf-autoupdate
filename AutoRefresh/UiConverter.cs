﻿namespace AutoRefresh
{
    public abstract class UiConverter
    {
        public abstract void Set(object model);
    }
}