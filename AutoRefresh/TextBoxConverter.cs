﻿using System.Windows;
using System.Windows.Controls;

namespace AutoRefresh
{
    public class TextBoxConverter : UiConverter
    {
        private readonly TextBox _uiElement;

        public TextBoxConverter(UIElement uiElement)
        {
            _uiElement = uiElement as TextBox;
        }

        public override void Set(object model)
        {
            _uiElement.Text = model.ToString();
        }
    }
}