﻿using System;

namespace AutoRefresh
{
    public class SomeBussinesObject
    {
        private string _timestamp;

        public void ChangeData()
        {
            _timestamp = DateTime.Now.ToShortTimeString();
        }

        public string SomePropperty
        {
            get { return "hello world"; }
        }
    }
}